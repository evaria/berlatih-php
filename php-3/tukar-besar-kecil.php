<?php
function tukar_besar_kecil($string){
    $numberOfString = strlen($string);
    $changechar = '';
    for($i = 0; $i < $numberOfString; $i++){
        $char = $string[$i];
        if(ctype_space($char)){
            $changechar .= ' ';
        }elseif(ctype_upper($char)){
            //echo $char;
            $lowercase = strtolower($char);
            $changechar .= $lowercase;
        }elseif(ctype_lower($char)===true) {
            $uppercase = strtoupper($char);
            $changechar .= $uppercase;
        }else{
            $changechar .= $char;
        }
        
    }
    return $changechar;
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "</br>";
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "</br>";
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "</br>";
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "</br>";
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>