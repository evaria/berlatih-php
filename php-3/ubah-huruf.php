<?php
function ubah_huruf($string){
//kode di sini
    $numberOfString = strlen($string);
    $changechar = '';
    for($i = 0; $i < $numberOfString; $i++){
        $char = $string[$i];
        $changechar .= ++$char;
    }
    return $changechar;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "</br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "</br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "</br>";
echo ubah_huruf('keren'); // lfsfo
echo "</br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>