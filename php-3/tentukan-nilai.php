<?php
function tentukan_nilai($number)
{
    //  kode disini
    if($number >= 85 && $number <= 100){
        return "Sangat Baik";
        //echo "Kategori nilai $number adalah <b>Sangat Baik</b> </br>";
    }elseif($number >= 70 && $number < 85){
        return "Baik";
        //echo "Kategori nilai $number adalah <b>Baik</b> </br>";
    }elseif($number >= 60 && $number < 70){
        return "Cukup";
        //echo "Kategori nilai $number adalah <b>Cukup</b> </br>";
    }else {
        return "Kurang";
        //echo "Kategori nilai $number adalah <b>Kurang</b> </br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "</br>";
echo tentukan_nilai(76); //Baik
echo "</br>";
echo tentukan_nilai(67); //Cukup
echo "</br>";
echo tentukan_nilai(43); //Kurang
?>