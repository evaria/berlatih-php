<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new animal('shaun');
echo "The animal's name: " . $sheep->get_name() . "</br>";
// Karena Shaun The Sheep berkaki dua jadi menggunakan nilai default :)
echo "The animal's legs: " . $sheep->get_legs() . "</br>";
echo "The animal's cold type: " . $sheep->get_cold_blooded() ."</br>";

echo "<br> Kelas Pewarisan";
echo "<br>";

$sungokong = new ape('kera sakti');
echo "The animal's name: " . $sungokong->get_name() . "</br>";
echo "The animal's legs: " . $sungokong->get_legs() . "</br>";
echo "The animal's cold type: " . $sungokong->get_cold_blooded() ."</br>";
echo "How this animal yell?: " . $sungokong->get_yell() ."</br>";

echo "<br>";

$kodok = new frog('buduk');
echo "The animal's name: " . $kodok->get_name() . "</br>";
echo "The animal's legs: " . $kodok->get_legs() . "</br>";
echo "The animal's cold type: " . $kodok->get_cold_blooded() ."</br>";
echo "How this animal jump?: " . $kodok->get_jump() . "</br>";


?>