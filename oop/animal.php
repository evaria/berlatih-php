<?php

class animal{
    
    public $name;

    //Default Property 
    public $legs=2;
    public $cold_blooded=false;
 
    //constructor name
    public function __construct($name){
        $this->name = $name;
    }

    public function set_name(){
        $this->name = $name;
    }

    public function get_name(){
        return $this->name;
    }

    public function get_legs(){
        return $this->legs;
    }

    public function get_cold_blooded(){
        if ($this) {
            return 'false';
        }else {
            return 'true';
        }

        return $this->cold_blooded;

    }
}


?>